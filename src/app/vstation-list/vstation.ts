/**
 * Velo'v station interface, as returned from an API call.
 * 
 * Example :
 * {
 *      "number":"19001",
 *      "pole":"dtgenis_lyon_sud",
 *      "available_bikes":"5",
 *      "code_insee":"69204",
 *      "lng":"4.7947728497160735",
 *      "availability":"Vert",
 *      "availabilitycode":"1",
 *      "etat":"None",
 *      "startdate":"None",
 *      "langue":"None",
 *      "bike_stands":"12",
 *      "last_update":"2020-04-18 23:01:44",
 *      "available_bike_stands":"7",
 *      "gid":"1263",
 *      "titre":"None",
 *      "status":"OPEN",
 *      "commune":"SAINT-GENIS-LAVAL",
 *      "description":"None",
 *      "nature":"None",
 *      "bonus":"",
 *      "address2":"None",
 *      "address":"49 Avenue Gorges Clémenceau",
 *      "lat":"45.6978120018869000",
 *      "last_update_fme":"2020-04-18 23:10:00",
 *      "enddate":"None",
 *      "name":"St Genis L. - Médiathèque",
 *      "banking":"True",
 *      "nmarrond":"None"
 *  }
 */


export interface VStation {
    number: number;
    gid: number;
    name: string;
    commune: string;
    nmarrond: number;
    bike_stands: number;
    status: number;
    available_bike_stands: number;
    available_bikes: number;
    availabilitycode: number;
    availibility: string;
    lat: string;
    lng: string;
    address: string;
    last_update: string;
    last_update_fme: string;
    titre?: string;
    description?: string;
    banking?: boolean;
    code_insee?: number;
    startdate?: string;
    enddate?: string;
    bonus?: string;
    pole?: string;
    langue?: string;
    etat?: string;
    nature?: string;
    address2?: string;
}

export interface VStationResponse {
    fields: string;
    nb_results: number;
    next: string;
    values: VStation[];
}