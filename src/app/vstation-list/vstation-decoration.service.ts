import { Injectable } from '@angular/core';
import { of } from 'rxjs';

/**
 *   Unsorted random useful stuff remotely related to VStation list sorting, 
 *   filtering, CSS decoration (based on sort or filter), change log message 
 *   decoration, etc...
 */


// ChangeLog messages criticity levels
export enum CLMsgLvl {
    USELESS = 1,
    INFO,
    WARNING,
    IMPORTANT,
    CRITICAL,
};


  /**
 * Service for getting an Observable on the list of Velo'v stations from 
 * download.data.grandlyon.com
 */

@Injectable({
    providedIn: 'root'
  })
export class VStationDecorationService {

    stationAvailStatusDecoration$ = of({
        0: "bg-danger",
        1: "bg-success",
        2: "bg-warning",
        3: "bg-info",
    });

    stationFilters$ = of([
        {'name': "All", 'value': -1},
        {'name': "Stands and bike available", 'value': 1},
        {'name': "Stands available only", 'value': 3},
        {'name': "Bikes available only", 'value': 2},
        {'name': "Station unavailable", 'value': 0},
    ]);
    
    stationSorts$ = of([
        // {'name': "Latest activity", 'value': "last_update"}, // not working currently
        {'name': "Available bikes", 'value': "available_bikes"},
        {'name': "Available stands", 'value': "available_bike_stands"}, 
    ]);

}
