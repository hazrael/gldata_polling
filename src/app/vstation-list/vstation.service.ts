import { Injectable } from '@angular/core';
import { tap, map, catchError, startWith, switchMap, shareReplay, takeLast, scan, retry, retryWhen, delay, take } from 'rxjs/operators';
import { VStationResponse, VStation } from './vstation';
import { HttpClient } from '@angular/common/http';
import { EMPTY, interval, Subject, combineLatest, of, merge, BehaviorSubject, ReplaySubject } from 'rxjs';
import { VStationDecorationService, CLMsgLvl } from './vstation-decoration.service';


/**
 * Service for getting an Observable on the list of Velo'v stations from 
 * download.data.grandlyon.com
 */

@Injectable({
  providedIn: 'root'
})
export class VStationService {

  private glBaseUrl = "https://download.data.grandlyon.com";
  private glSubUrl = "ws/rdata/jcd_jcdecaux.jcdvelov/all.json";
  private glParams = "maxfeatures=450&start=1";
  private glDataUrl: string = `${this.glBaseUrl}/${this.glSubUrl}?${this.glParams}`;
  
  private tick = 300000;      // tick every 5 minutes by default

  /** 
   *  Several subjects for user interaction (from vstation-list component)
   */
  private refreshSubject = new Subject<Date>();
  refreshAction$ = this.refreshSubject.asObservable();

  private filterSubject = new BehaviorSubject<number>(-1);
  filterAction$ = this.filterSubject.asObservable();

  private nameFilterSubject = new BehaviorSubject<string>("");
  nameFilterAction$ = this.nameFilterSubject.asObservable();

  private sortSubject = new BehaviorSubject<string>("available_bikes");
  sortAction$ = this.sortSubject.asObservable();

  private previousStationList: VStation[] = [];
  private changeLogSubject = new ReplaySubject<VStation[]>(1); // needs explaining, I'm not sure why it works better than a regular subject in this case (but it sure does).
  changeLog$ = this.changeLogSubject.asObservable().pipe(tap(() => console.log("In init changeLog$ tap")));


  constructor(private httpClient: HttpClient, 
              private decoService: VStationDecorationService) {}

  // Observable on HTTP Response for Velov Stations.
  // - Retry up to 2 times with 3s delay between each try in case the HTTP GET fails.
  // - Map HTTP Response to keep only a VStation[] array.
  // - Tap the data to the changeLog observable for further processing 
  stations$ = this.httpClient.get(this.glDataUrl).pipe(
    retryWhen(errors => errors.pipe(
      delay(3000), 
      take(2),
      tap(() => console.log("Error while fetching data, even after retrie"))
    )),
    map((response: VStationResponse) => response.values),
    tap((stations) => {
      console.log("New item emitted by httpClient (and re-emitted to changeLogSubject)");
      this.changeLogSubject.next(stations);
    }),
    catchError(this.handleError),
  );
    

  // Triggers either on a regular tick or manually  - emits a refresh Date() object
  refreshWithManual$ = merge(
    interval(this.tick).pipe(
      startWith(new Date()),
      map(() => new Date()), 
      tap(() => console.log('Refreshing from interval')),
    ),                          
    this.refreshAction$.pipe(
      tap(() => console.log('Refreshing from user action')),
    )
  ).pipe(
    catchError(this.handleError)
  );


  // 'Final' observable on station list, triggered either by a tick or by a user action.
  stationsWithRefresh$ = this.refreshWithManual$.pipe(
    switchMap((refreshDate) => this.stations$.pipe(
      map((stations) => ({
            'stations': stations,
            'refreshDate': refreshDate.toLocaleTimeString()
      })),
      tap((d) => 
        console.log(`Item emitted by stationsWithRefresh$ : got ${d.stations.length} stations`)),
      shareReplay(1),
      tap((data) => console.log(`Item emitted by stationsWithRefresh$ after shareReplay`))
    ))
  );

  stationsWithRefreshFilter$ = combineLatest(
    this.stationsWithRefresh$,
    this.filterAction$.pipe(tap(() => console.log("In filterAction")))
  ).pipe(
    map(([stationWithRefresh, filterStatus]: [any, number]) => ({
        'stations': stationWithRefresh.stations.filter(
          station => +filterStatus !== -1 ? station.availabilitycode === filterStatus : true
        ),
        'refreshDate': stationWithRefresh.refreshDate
      }))
  );

  stationsWithRefreshNameFilter$ = combineLatest(
    this.stationsWithRefreshFilter$,
    this.nameFilterAction$.pipe(tap(() => console.log("In filterAction")))
  ).pipe(
    map(([stationWithRefresh, filterValues]: [any, string]) => ({
        'stations': stationWithRefresh.stations.filter(
          station => station.name.toLocaleLowerCase().indexOf(filterValues) !== -1
        ),
        'refreshDate': stationWithRefresh.refreshDate
      }))
  );

  stationsWithRefreshFilterAndSort$ = combineLatest(
    this.stationsWithRefreshNameFilter$,
    this.sortAction$.pipe(tap(() => console.log("In sortAction")))
  ).pipe(
    tap((data) => console.log(`Received ${data[0].stations.length} stations`)),
    map(([stationWithRefreshF, sortCategory]: [any, string]) => ({
        'stations': stationWithRefreshF.stations.sort(
          (s1, s2) => +s2[sortCategory] - +s1[sortCategory]),
        'refreshDate': stationWithRefreshF.refreshDate
      }))
  );

  enhancedStationsWithRefreshFilterSort$ = combineLatest(
    this.stationsWithRefreshFilterAndSort$,
    this.decoService.stationAvailStatusDecoration$
  ).pipe(
    tap((data) => console.log(`[Enhanced] : Received ${data[0].stations.length} stations`)),
    map(([enStationWithRFS, decoration]) => ({
      'stations': enStationWithRFS.stations.map(
        station => ({
          'decoration': decoration[station.availabilitycode],
          ...station
        })
      ),
      'refreshDate': enStationWithRFS.refreshDate
    }),
  ));


  changeLogDiff$ = this.changeLog$.pipe(
    tap((data) => console.log(`In changeLogDiff$ - Received ${data.length} stations and there are ${this.previousStationList.length} stations in previousStationList`)),
    map((newStations) => {

      let diffArray: any[] = [];

      if (this.previousStationList.length === 0) {
        this.previousStationList = newStations;
        diffArray.push({
          'msg': "No refresh of data yet",
          'lvl': CLMsgLvl.USELESS
        });
        return diffArray;
      }

      let oldStations = {};
      for (let station of this.previousStationList) {
        oldStations[station.name] = station;
      }

      for (let newStation of newStations) {
        diffArray = diffArray.concat(this.diffStation(oldStations[newStation.name], newStation));
      }
      
      this.previousStationList = newStations;
      
      diffArray.push({
        'msg': `New diff at ${new Date().toLocaleTimeString()} (${diffArray.length} changes):`,
        'lvl': CLMsgLvl.IMPORTANT,
      });
      return diffArray;
    }),
    scan((acc, newCL) => {
      acc = acc.concat(newCL);
      if (acc.length > 75) {
        acc.splice(0, acc.length - 75);
      }
      return acc;
    })
  )

  diffStation(vs1: VStation, vs2: VStation): string[] {

    let diff: any[] = [];
    
    if (vs1.number !== vs2.number) {
        console.log("Warning, diff is expected data the same station at \
        different times. Received two different stations instead.");
        return [];
    }

    const name = vs1.name;

    if (vs1.last_update !== vs2.last_update) {
      // diff.push(`New update for station ${name}`);
    }

    if (+vs1.available_bikes > +vs2.available_bikes) {
      diff.push({
        'msg': `Bike(s) removed from station ${name}`, 
        'lvl': CLMsgLvl.WARNING
      });
    } else if (+vs1.available_bikes < +vs2.available_bikes) {
      diff.push({
        'msg': `Bike(s) put back in station ${name}`,
        'lvl': CLMsgLvl.INFO
      });
    }

    if (+vs1.available_bikes !== 0 && +vs2.available_bikes === 0) {
      diff.push({
        'msg': `No more bikes in station ${name}`,
        'lvl': CLMsgLvl.CRITICAL
      });
    } else if (+vs1.available_bike_stands !== 0 && +vs2.available_bike_stands === 0) {
      diff.push({
        'msg': `No more bikes stands in station ${name}`,
        'lvl': CLMsgLvl.CRITICAL
      });
    } else if (+vs1.available_bike_stands === 0 && +vs2.available_bike_stands !== 0) {
      diff.push({
        'msg': `Bikes stands now available in station  ${name}`,
        'lvl': CLMsgLvl.INFO
      });
    } else if (+vs1.available_bikes === 0 && +vs2.available_bikes !== 0) {
      diff.push({
        'msg': `Bikes now available in station  ${name}`,
        'lvl': CLMsgLvl.INFO
      });
    }
    return diff;
  } 

  filterStationByName(filterVal: string): any {

    // filterVal = filterVal.toLocaleLowerCase();
    // let tempFiltered = {};

    // for ( let [key, value] of Object.entries(this.currentStations)) {
    //   if (value['name'].toLocaleLowerCase().indexOf(filterVal) !== -1) {
    //     tempFiltered[key] = value;
    //   }
    // }
    // return tempFiltered;
  }

  handleError(err) {
    console.log(`Error in VStation Service : ${err}`);
    return EMPTY;
  }

  emitManualRefresh(refreshDate: Date): void {
    this.refreshSubject.next(refreshDate);
  }

  emitFilter(filterStatus: number): void {
    console.log(`Emitting filter : ${filterStatus}`);
    this.filterSubject.next(filterStatus);
  }

  emitNameFilter(filterValue: string): void {
    console.log(`Emitting filter : ${filterValue.toLocaleLowerCase()}`);
    this.nameFilterSubject.next(filterValue.toLocaleLowerCase());
  }

  emitSort(sortStatus: string): void {
    console.log(`Emitting sort : ${sortStatus}`);
    this.sortSubject.next(sortStatus);
  }
}
