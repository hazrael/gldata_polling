import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'vstation',
  templateUrl: './vstation.component.html',
  // styleUrls: ['./vstation.component.css']
})
export class VstationComponent implements OnInit {

  @Input() station: any;

  collapseId: string;
  dcollapseId: string;

  constructor() { }

  ngOnInit(): void {
    this.collapseId = `collapse${this.station.number}`;
    this.dcollapseId = `#${this.collapseId}`;
  }

}
