import { Component, OnInit, AfterViewInit } from '@angular/core';
import { VStationService } from './vstation.service';
import { Subject, EMPTY } from 'rxjs';
import { catchError, map,  } from 'rxjs/operators';
import { VStation } from './vstation';
import { faSync } from '@fortawesome/free-solid-svg-icons';

import { Pipe, PipeTransform } from '@angular/core';
import { VStationDecorationService, CLMsgLvl } from './vstation-decoration.service';

@Pipe({ name: 'reverse' })

export class ReversePipe implements PipeTransform {
  transform(value) {
    return value.slice().reverse();
  }
}

@Component({
  selector: 'vstation-list',
  templateUrl: './vstation-list.component.html',
  // styleUrls: ['./vstation-list.component.css']
})
export class VstationListComponent {

  cardHeader: string = "Bikes stations in Lyon";
  stationList: VStation[];
  faSync = faSync;  // Fontawesome sync icon
  

  private _errorMessageSubject = new Subject<string>();
  errorMessage$ = this._errorMessageSubject.asObservable();

  // Observable on the list of stations returned by download.data.grandlyon.com
  stations$ = this.stationService.enhancedStationsWithRefreshFilterSort$.pipe(
    catchError(err => this.handleError(err))
  );

  // Observable on filter types list ('only bikes available', 'only stands available' etc)
  stationFilters$ = this.decoService.stationFilters$.pipe(
    catchError(err => this.handleError(err))
  );

  // Observable on available sort keys (number of available bikes, stands, etc)
  stationSorts$ = this.decoService.stationSorts$.pipe(
    catchError(err => this.handleError(err))
  );

  // List of changes between current stationlist and N (currently 1) station lists
  changeLog$ = this.stationService.changeLogDiff$.pipe(
    map((changelog) => 
      changelog.map((lineLvl) => 
        ({'msg': lineLvl.msg, 'bg': this.lvlToBg(lineLvl.lvl)}))
    ),
    catchError(err => this.handleError(err))
  );

  constructor(private stationService: VStationService, 
              private decoService: VStationDecorationService) {}

  handleError(err: string) {
    this._errorMessageSubject.next(err);
    return EMPTY;
  }

  manualRefresh(): void {
    this.stationService.emitManualRefresh(new Date());
  }

  filterList(selected): void {
    this.stationService.emitFilter(selected);
  }

  filterByName(text): void {
    console.log(`Change in input text : ${text}`);
    this.stationService.emitNameFilter(text);
  }

  sortList(selected): void {
    this.stationService.emitSort(selected);
  }

  lvlToBg(level: CLMsgLvl): string {
    switch(level) { 
      case CLMsgLvl.USELESS: { 
         return "#FAFAFA";
      } 
      case CLMsgLvl.INFO: { 
        return "#4FC3F7";
      } 
      case CLMsgLvl.WARNING: { 
        return "#FFF176";
      } 
      case CLMsgLvl.IMPORTANT: { 
        return "#A569BD ";
      } 
      case CLMsgLvl.CRITICAL: { 
        return "#EF5350";
      } 
      default: { 
        return "#FAFAFA";
      } 
   }
  }

}
