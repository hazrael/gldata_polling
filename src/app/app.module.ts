import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { VstationListComponent, ReversePipe } from './vstation-list/vstation-list.component';
import { HttpClientModule } from '@angular/common/http';
import { VstationComponent } from './vstation-list/vstation.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MapComponent } from './map/map.component';

@NgModule({
  declarations: [
    AppComponent,
    VstationListComponent,
    VstationComponent,
    ReversePipe,
    MapComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FontAwesomeModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
