import { Component, AfterViewInit } from '@angular/core';
import * as L from 'leaflet';
import { tap } from 'rxjs/operators';
import { MapService } from './map.service';
import { token } from '../mapbox_token';

@Component({
  selector: 'map-display',
  templateUrl: './map.component.html',
})
export class MapComponent implements AfterViewInit {

  private map: L.Map;
  defaultZoom: number = 13;
  defaultCenter: L.latLng = [ 45.7597, 4.8422 ]; // Centered on Lyon
  private _accessToken: string = token;
  markerGroup = L.layerGroup();

  mapData$ = this.mapService.tsStationMarkers$.pipe(
    tap(ts_stations => this.updateMarkers(ts_stations.markers))
  );

  constructor(private mapService: MapService) { }

  ngAfterViewInit(): void {
    
    this.initMap();

    this.mapData$.subscribe();

  }

  private initMap(): void {

    // Create and center map 
    this.map = L.map('map', {
      center: this.defaultCenter,
      zoom: this.defaultZoom,
      zoomControl: false,
    });

    // Add tile layer from Mapbox
    L.tileLayer(
      'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', 
      {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 24,
        id: 'nicolasceska/ck95ga1gw04ho1hnn22xopscd',
        accessToken: this._accessToken,
        tileSize: 512,
        zoomOffset: -1,
      }
    ).addTo(this.map);
  }

  updateMarkers(markers): void {

    let newMarkerGroup = L.layerGroup();

    console.log("In updateMarkers()");
    for (let stationMarker of markers) {
      stationMarker.on('click', 
        this.onStationMarkerClicked
      );
      stationMarker.bindPopup(this.generatePopup, {"autoPan": false});
      newMarkerGroup.addLayer(stationMarker);
    }
    
    // remove old markers
    this.markerGroup.clearLayers();

    this.markerGroup = newMarkerGroup;
    this.markerGroup.addTo(this.map);

    if (markers.length == 1) {
      console.log("Unique marker : ", markers[0]);
      this.map.panTo(markers[0]._latlng);
      markers[0].togglePopup();
    }
  }

  onStationMarkerClicked(event: L.MouseEvent): void {  
    console.log("Marker clicked :", event);
    // why isn't this.map accessible here ?
    event.target._map.panTo(event.target._latlng);
    event.target.togglePopup();
  }

  generatePopup(layer: L.Layer): string {
    console.log(layer);
    const s = layer.station;
    let innertext = `<h6>${s.name}</h6><hr><p>B:${s.available_bikes} / S:${s.available_bike_stands}</p>`;
    return innertext;
  }
}