import { Injectable } from '@angular/core';
import { VStationService } from '../vstation-list/vstation.service';
import { catchError, map } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import * as L from 'leaflet';
import { VStationMarker } from './marker';
import { VStation } from '../vstation-list/vstation';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  constructor(private stationService: VStationService) { }

  stations$ = this.stationService.enhancedStationsWithRefreshFilterSort$.pipe(
    catchError(err => this.handleError(err))
  );

  tsStationMarkers$ = this.stations$.pipe(
    map((ts_stations) => ({
      'markers': ts_stations.stations.map((station: VStation) => 
          new VStationMarker(
              [+station.lat, +station.lng], 
              {
                color: this.colorizeMarker(+station.availabilitycode), 
                weight: 2, 
                radius: 6,
                fillOpacity: 0.5
              },
              station,
              false,
          ), 
        ),
      'refreshDate': ts_stations.refreshDate,
    })
  ));
  
  colorizeMarker(availCode: number): string{
    switch (availCode) {
      case 0: // Unavail
        return "#E74C3C";
      case 1: //  Bikes & stands
        return "#5DDE03";
      case 2: // Bikes only
        return "#FFD500";
      case 3: // Stands only
        return "#0F87FF";
      default:  // Grey for undetermined state
        return "#CCD1D1";
    }
  }

  handleError(err) {
      console.log("Error in station$ in map.service.ts");
    //this._errorMessageSubject.next(err);
    return EMPTY;
  }
}
