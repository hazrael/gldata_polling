import * as L from 'leaflet';
import { VStation } from '../vstation-list/vstation';


/**
 *  Simple Circle marker with station data
 */
export class VStationMarker extends L.CircleMarker {

    constructor(
        latlng: L.LatLng, options: L.CircleMarker_options, 
        public station: VStation, public selected: boolean) {
        super(latlng, options);
    }

}