export interface AvailColor {
    code: number;
    color: string;
}