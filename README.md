# Polling Opendata from data.grandlyon/com

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

### TL;DR :

Polls the open data at data.grandlyon.com for all Velo'v stations. Display them as a list and on a map (using leaflet).

This app brings nothing new, it's just an incentive for me to learn Angular.

Provides the following features :

- filter stations by name or availibility status (has bikes / has stands / has both / has none) (both in list and on map)
- sort station list by number of bikes or number of stands
- automatically refresh the data every 5min or on user action
- provides a changelog of the last bikes removed / placed back from/to stations
- display stations on map with color code based on availibilty and pan to station/display popup when user clicks on station marker

### Roadmap (note to myself) :

- switch from 'raw' bootstrap to ngx-bootstrap or angular materials
- rework popups to make them pretty
- add e2e tests
- add notifications (refresh, errors, sort/filter results, etc) with toastr/other?
- rework list display using pagination / routing ?
